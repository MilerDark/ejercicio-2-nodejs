const { response , request } = require('express')
const Personas = require('../data/data')
const Persona = require('../models/persona')

const dataPersonas = new Personas()

const personaGetdata = (req = request , res = response) => {

  const { nombres, apellidos} = req.query;
  if (nombres || apellidos) {
    let datosfilter = dataPersonas.filtrarPersonas(nombres, apellidos);
    res.json({
      datosfilter
    })
  } else {
    let datos = dataPersonas.getPersonas()
    res.json({
      datos
    })
  }
}

const personaGetdata1 = (req = request , res = response) => {

  const {dates} = req.params

  if (dates === "Masculino" || dates === "Femenino") {
    const dato = dataPersonas.data.filter(elemento => elemento.sexo === dates)
    res.json({
      dato
    })
  } else {
    const dato2 = dataPersonas.data.filter(elemento => elemento.ci === dates)
    
    if (dato2.length !== 0) {
      res.json({
        dato2
      })
    } else {
      res.json({
        msg: "Introduce el ci o el sexo"
      })
    }
  }
}

const personaGetdata2 = (req = request , res = response) => {

  const {inicial, sexo} = req.params
  const dato = dataPersonas.data.filter(elemento => elemento.sexo === sexo && elemento.nombres.charAt(0) === inicial)

  if (dato.length !== 0) {
    res.json({
      dato
    })
  } else {
    res.json({
      msg: "Datos no Encontrados"
    })
  }
}


const personaPost = (req = request, res = response) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  let personaNueva = new Persona(nombres, apellidos, ci, direccion, sexo)

  dataPersonas.addPersona(personaNueva)

  res.json({
    msg: 'Persona agregada exitosamente',
    personaNueva
  })
}

const personaPut = (req = request, res = response) => {
  const { id } = req.params
  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const index = dataPersonas.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPersonas.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      nombres,
      apellidos,
      ci,
      direccion,
      sexo,
      msg: 'Person Was Modified Sucessfully'
    })
  } else {
    res.json({
      msg: 'Person Not Found'
    })
  }
}

const personaDelete = (req = request, res = response) => {
  const {id} = req.params
  const index = dataPersonas.data.findIndex(object => object.id === id)
  if (index !== -1) {
    dataPersonas.eliminarPersona(index)
    res.json({
      msg: 'The Person Was Sucessfully Removed'
    })
  } else {
    res.json({
      msg: 'Person Not Found'
    })
  }
}

module.exports = {
  personaGetdata,
  personaGetdata1,
  personaGetdata2,
  personaPost,
  personaPut,
  personaDelete
}