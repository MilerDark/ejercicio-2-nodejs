const { Router } = require('express')
const { personaGetdata, personaGetdata1, personaGetdata2, personaPost, personaPut, personaDelete } = require('../controllers/personas')

const route = Router()

route.get('/', personaGetdata)
route.get('/:dates',personaGetdata1)
route.get('/:inicial/:sexo',personaGetdata2)
route.post('/', personaPost)
route.put('/:id', personaPut)
route.delete('/:id', personaDelete)

module.exports = route